#include <stdio.h>
#include "util.h"
#include "messages.h"
#include "event_q.h"

void printLegend()
{
    fprintf(stderr, "Legend:\r\n");
    fprintf(stderr, "'g' - Gets version from Nucleo\r\n");
    fprintf(stderr, "'s' - Sets compute parameters\r\n");
    fprintf(stderr, "'1' - Begins the computation of the Julia set\r\n");
    fprintf(stderr, "'a' - Interrupts current computation\r\n");
    fprintf(stderr, "'r' - Resets chunk_id\r\n");
    fprintf(stderr, "'l' - Deletes current results of compuation\r\n");
    fprintf(stderr, "'p' - Redraws the Julia set with the current results\r\n");
    fprintf(stderr, "'c' - Calculates the fractal on PC\r\n");
    fprintf(stderr, "'e' - Sets the size of the picture. Default: 320x240\r\n");
    fprintf(stderr, "'q' - Exits the program\r\n");
    fprintf(stderr, "'w' - Saves Julia set as jpeg into output.jpg\r\n");
}

message readParam()
{
    message msg;
    msg.type = MSG_SET_COMPUTE;
    fprintf(stderr, "INFO: Enter Re(c)\r\n");
    if (!scanf("%lf", &msg.data.set_compute.c_re))
    {
        fprintf(stderr, "ERROR: Wrong input, set_parameters aborted\r\n");
        msg.type = MSG_NBR;
        return msg;
    }
    fprintf(stderr, "INFO: Enter Im(c)\r\n");
    if (!scanf("%lf", &msg.data.set_compute.c_im))
    {
        fprintf(stderr, "ERROR: Wrong input, set_parameters aborted\r\n");
        msg.type = MSG_NBR;
        return msg;
    }
    fprintf(stderr, "INFO: Enter x-coordinates increment\r\n");
    if (!scanf("%lf", &msg.data.set_compute.d_re))
    {
        fprintf(stderr, "ERROR: Wrong input, set_parameters aborted\r\n");
        msg.type = MSG_NBR;
        return msg;
    }
    fprintf(stderr, "INFO: Enter y-coordinates increment\r\n");
    if (!scanf("%lf", &msg.data.set_compute.d_im))
    {
        fprintf(stderr, "ERROR: Wrong input, set_parameters aborted\r\n");
        msg.type = MSG_NBR;
        return msg;
    }
    fprintf(stderr, "INFO: Enter number of iterations\r\n");
    if (!scanf("%hhd", &msg.data.set_compute.n))
    {
        fprintf(stderr, "ERROR: Wrong input, set_parameters aborted\r\n");
        msg.type = MSG_NBR;
        return msg;
    }
    fprintf(stderr, "INFO: Parameters set successfully - Re(c) = %f, Im(c) = %f, x-increment = %f, y-increment = %f, iterations %d\r\n", msg.data.set_compute.c_re, msg.data.set_compute.c_im, msg.data.set_compute.d_re, msg.data.set_compute.d_im, msg.data.set_compute.n);
    return msg;
}

message getParamCompute()
{
    message msg;

    fprintf(stderr, "INFO: Enter real part of start point\r\n");
    if (scanf("%lf", &msg.data.compute.re) != 1)
    {
        fprintf(stderr, "ERROR: Wrong input, set_parameters aborted\r\n");
    }
    fprintf(stderr, "INFO: Enter imaginary part of start point\r\n");
    while (scanf("%lf", &msg.data.compute.im) != 1)
    {
        fprintf(stderr, "ERROR: Wrong input, set_parameters aborted\r\n");
    }
    return msg;
}

int getR(double t)
{
    double r = 9 * (1 - t) * t * t * t * 255;
    r = r < 255 ? r : 255;
    return (int)r;
}

int getG(double t)
{
    double g = 15 * (1 - t) * (1 - t) * t * t * 255;
    g = g < 255 ? g : 255;
    return (int)g;
}

int getB(double t)
{
    double b = 8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255;
    b = b < 255 ? b : 255;
    return (int)b;
}