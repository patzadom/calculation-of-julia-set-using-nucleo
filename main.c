//
//  Author: Dominik Patzak
//
#include <stdlib.h>
#include "messages.h"
#include "xwin_sdl.h"
#include "util.h"
#include "event_q.h"
#include "save_jpeg.h"
#include "prg_serial_nonblock.h"
#include <termios.h>
#include <stdbool.h>
#include <poll.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <pthread.h>
#define THREAD_COUNT 3
#define SERIAL_NAME "/dev/ttyACM0"
#define BUF_SIZE 512
void *keyboardThread(void *v); // reads input from keyboard
void *serialThread(void *v);   // reads input from nucleo
void *computeThread(void *v);
void sendMessage(message *msg, uint8_t *buf); //sends message to nucleo
void printLegend();                           // prints legend of each characters at start
int getR(double t);                           // returns R value of pixel
int getG(double t);                           // returns G value of pixel
int getB(double t);                           // returns B value of pixel
message readParam();                          // reads set_compute parameters
message getComputeParam();                    //re
volatile int rx_in;
volatile int rx_out;
char rx_buffer[BUF_SIZE];
pthread_mutex_t mtx;
pthread_mutex_t mtx_compute;
pthread_cond_t push_ok;
pthread_cond_t receive_compute_data;
typedef struct
{
    queue *queue;
    bool quit;
    int chunk_id;
    bool computing;
    int iteration_number;
    int idx;
    int row;
    int column;
    bool window;
    double step_re;
    double step_im;
    double start_re;
    double start_im;
} Data;
Data initData()
{
    Data ret;
    ret.queue = create_queue();
    ret.quit = false;
    ret.chunk_id = 0;
    ret.iteration_number = 0;
    ret.row = 240;
    ret.window = false;
    ret.step_re = 0;
    ret.step_im = 0;
    ret.idx = 0;
    ret.start_im = ret.start_re = 0;
    ret.computing = false;
    ret.column = 320;
    return ret;
}
int main() // boss-worker model
{
    // set terminal to raw mode
    struct termios tio, tioOld;
    tcgetattr(STDIN_FILENO, &tioOld);
    cfmakeraw(&tio);
    tcsetattr(STDIN_FILENO, TCSANOW, &tio);

    // init shared data
    Data sharedData = initData();
    bool r = false;
    bool event_quit = false;
    bool keyboard_quit = false;
    bool serial_quit = false;
    bool compute_quit = false;
    pthread_mutex_init(&mtx, NULL);
    pthread_mutex_init(&mtx_compute, NULL);
    pthread_cond_init(&push_ok, NULL);
    pthread_cond_init(&receive_compute_data, NULL);
    // create threads
    pthread_t thrs[THREAD_COUNT];
    r = pthread_create(&thrs[0], NULL, keyboardThread, &sharedData);
    fprintf(stderr, "INFO: Create thread '%s' %s\r\n", "keyboardThread", (r == 0 ? "OK" : "FAIL"));
    r = pthread_create(&thrs[1], NULL, serialThread, &sharedData);
    fprintf(stderr, "INFO: Create thread '%s' %s\r\n", "serialThread", (r == 0 ? "OK" : "FAIL"));
    r = pthread_create(&thrs[0], NULL, computeThread, &sharedData);
    fprintf(stderr, "INFO: Create thread '%s' %s\r\n", "computeThread", (r == 0 ? "OK" : "FAIL"));
    printLegend();
    uint8_t *image = NULL;
    while (!event_quit)
    {
        pthread_mutex_lock(&mtx);
        pthread_cond_signal(&push_ok);
        event ev = pop(sharedData.queue);
        pthread_mutex_unlock(&mtx);
        if (ev.type != EV_NULL)
        {
            uint8_t *send_buffer = malloc(sizeof(message));
            pthread_mutex_lock(&mtx);
            switch (ev.type) // process events
            {
            case EV_SET_SIZE:
                if (!sharedData.computing)
                {
                    sharedData.row = ev.msg.data.size.height;
                    sharedData.column = ev.msg.data.size.width;
                    fprintf(stderr, "INFO: Size set to %dx%d\r\n", sharedData.column, sharedData.row);
                }
                break;
            case EV_SAVE_IMAGE:
                if (image != NULL)
                {
                    fprintf(stderr, "Image saved to 'output.jpg'\r\n");
                    save_image_jpeg(sharedData.column, sharedData.row, image, "output.jpg");
                }
                else
                {
                    fprintf(stderr, "ERROR: Cannot save image, no computation was done yet\r\n");
                }
                break;
            case EV_REDRAW:
                if (image != NULL)
                {
                    if (!sharedData.window)
                    {
                        xwin_init(sharedData.column, sharedData.row);
                        sharedData.window = true;
                    }
                    xwin_redraw(sharedData.column, sharedData.row, image);
                    delay(10);
                    xwin_redraw(sharedData.column, sharedData.row, image);
                }
                else
                {
                    fprintf(stderr, "Error, cannot show image");
                }
                break;
            case EV_DELETE_RESULT:
                if (image != NULL)
                {
                    for (int row = 0; row < sharedData.row; row++)
                    {
                        for (int column = 0; column < sharedData.column * 3; column++)
                        {
                            image[row * sharedData.row + column] = 0;
                        }
                    }
                }
                break;
            case EV_ABORT_NUCLEO:
                fprintf(stderr, "INFO: Abort from Nucleo\r\n");
                sharedData.computing = false;
                break;
            case EV_COMPUTE_THREAD:
                compute_quit = true;
                fprintf(stderr, "INFO: Call join to the thread %s\r\n", "computeThread");
                break;
            case EV_ABORT_KEYBOARD:
                if (!sharedData.computing)
                {
                    fprintf(stderr, "WARN: Abort requested but it is not computing\n\r");
                }
                else
                {
                    fprintf(stderr, "INFO: Abort requested\r\n");
                    sendMessage(&ev.msg, send_buffer);
                }
                sharedData.computing = false;
                break;
            case EV_VERSION:
                fprintf(stderr, "INFO: Nucleo firmware ver. %d.%d\r\n", (ev.msg).data.version.major, (ev.msg).data.version.minor);
                break;
            case EV_GET_VERSION:
                fprintf(stderr, "INFO: Get version requested\r\n");
                sendMessage(&ev.msg, send_buffer);
                break;
            case EV_KEYBOARD:
                keyboard_quit = true;
                fprintf(stderr, "INFO: Call join to the thread %s\r\n", "keyboardThread");
                break;
            case EV_SERIAL:
                serial_quit = true;
                fprintf(stderr, "INFO: Call join to the thread %s\r\n", "serialThread");
                break;
            case EV_OK:
                //fprintf(stderr, "INFO: Receive ok from Nucleo\r\n");
                break;
            case EV_COMPUTE_DATA_BURST:
                ;
                fprintf(stderr, "INFO: New computation chunk id: %d of size %d\r", ev.msg.data.compute_data_burst.chunk_id, ev.msg.data.compute_data_burst.length);
                for (int i = 0; i < ev.msg.data.compute_data_burst.length; i++)
                {
                    double t = (double)ev.msg.data.compute_data_burst.iters[i] / (double)sharedData.iteration_number;
                    image[sharedData.idx * 3 + i * 3] = getR(t);
                    image[sharedData.idx * 3 + i * 3 + 1] = getG(t);
                    image[sharedData.idx * 3 + i * 3 + 2] = getB(t);
                }
                sharedData.chunk_id++;
                sharedData.idx += ev.msg.data.compute_data_burst.length;
                if (sharedData.idx == sharedData.row * sharedData.column)
                {
                    sharedData.computing = false;
                    printf("Computation Finished\r\n");
                }
                else
                {
                    pthread_cond_signal(&receive_compute_data);
                }
                break;
            case EV_DONE:
                fprintf(stderr, "INFO: Nucleo reports the computation is done computing\r\n");
                sharedData.computing = false;
                if (!sharedData.window)
                {
                    xwin_init(sharedData.row, sharedData.column);
                }
                delay(1);
                xwin_redraw(sharedData.row, sharedData.column, image);
                delay(1);
                xwin_redraw(sharedData.row, sharedData.column, image);
                break;
            case EV_ERROR:
                fprintf(stderr, "WARN: Receive error from Nucleo\r\n");
                break;
            case EV_QUIT:
                fprintf(stderr, "INFO: Call to quit\r\n");
                message msg = {.type = MSG_ABORT};
                pthread_cond_signal(&receive_compute_data);
                sendMessage(&msg, send_buffer);
                break;
            case EV_SET_COMPUTE:
                sharedData.iteration_number = ev.msg.data.set_compute.n;
                sharedData.step_re = ev.msg.data.set_compute.d_re;
                sharedData.step_im = ev.msg.data.set_compute.d_im;
                sendMessage(&ev.msg, send_buffer);
                break;
            case EV_COMPUTE:
                if (!sharedData.computing)
                {
                    image = malloc(sizeof(uint8_t) * sharedData.column * sharedData.row * 3);
                    sharedData.start_re = ev.msg.data.compute.re;
                    sharedData.start_im = ev.msg.data.compute.im;
                    if (image == NULL)
                    {
                        fprintf(stderr, "ERROR: Cannot allocate memory, discarding compute data\r\n");
                    }
                    else
                    {
                        sharedData.computing = true;
                        pthread_cond_signal(&receive_compute_data);
                    }
                }
                else
                {
                    fprintf(stderr, "WARN: New computation requested but it is discarded due on ongoing computation\n\r");
                }
                break;
            case EV_RESET:
                if (!sharedData.computing)
                {
                    fprintf(stderr, "INFO: Chunk reset request\n\r");
                    sharedData.chunk_id = 0;
                }
                else
                {
                    fprintf(stderr, "WARN: Chunk reset request discarded, it is currently computing\n\r");
                }
                break;
            case EV_STARTUP:
                fprintf(stderr, "INFO: Nucleo restarted - '%s'\r\n", ev.msg.data.startup.message);
                sharedData.computing = false;
                break;
            default:
                break;
            }
            pthread_mutex_unlock(&mtx);
            if (serial_quit && keyboard_quit && compute_quit)
            {
                event_quit = true;
            }
            free(send_buffer);
        }
    }

    for (int i = 0; i < THREAD_COUNT; ++i)
    {
        pthread_join(thrs[i], NULL);
    }
    clear_queue(sharedData.queue);
    // restore terminal settings
    tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
    free(image);
    printf("\rMain thread finished\n");
    return 0;
}
void *keyboardThread(void *v)
{
    fflush(stdout);
    Data *sharedData = (Data *)v;
    bool q = false;
    while (!q)
    {
        char ch = getchar();
        fflush(stdout);
        pthread_mutex_lock(&mtx);
        pthread_cond_wait(&push_ok, &mtx);
        fflush(stdout);
        message msg;
        event ev;
        switch (ch)
        {
        case 'g':
            msg.type = MSG_GET_VERSION;
            ev.type = EV_GET_VERSION;
            ev.msg = msg;
            push(sharedData->queue, ev);
            break;
        case '1':
            msg = getParamCompute();
            ev.type = EV_COMPUTE;
            ev.msg = msg;
            push(sharedData->queue, ev);
            break;
        case 'a':
            msg.type = MSG_ABORT;
            ev.type = EV_ABORT_KEYBOARD;
            ev.msg = msg;
            push(sharedData->queue, ev);
            break;
        case 'r':
            msg.type = MSG_NBR;
            ev.type = EV_RESET;
            ev.msg = msg;
            push(sharedData->queue, ev);
            break;
        case 'q':
            msg.type = MSG_NBR;
            ev.type = EV_QUIT;
            ev.msg = msg;
            push(sharedData->queue, ev);
            sharedData->quit = true;
            break;
        case 'l':
            ev.type = EV_DELETE_RESULT;
            push(sharedData->queue, ev);
            break;
        case 'p':
            ev.type = EV_REDRAW;
            push(sharedData->queue, ev);
            break;
        case 's':
            msg = readParam();
            if (msg.type == MSG_NBR)
            {
                ev.type = EV_NULL;
            }
            else
            {
                ev.type = EV_SET_COMPUTE;
            }
            ev.msg = msg;
            push(sharedData->queue, ev);
            break;
        case 'w':
            ev.type = EV_SAVE_IMAGE;
            push(sharedData->queue, ev);
            break;
        case 'e':
            fprintf(stderr, "INFO: Enter size (width height)\r\n");
            int row;
            int column;
            if (scanf("%d %d", &column, &row) != 2)
            {
                fprintf(stderr, "ERROR: Wrong input type, aborting\r\n");
            }
            else if (!sharedData->computing)
            {
                ev.type = EV_SET_SIZE;
                msg.data.size.width = column;
                msg.data.size.height = row;
                ev.msg = msg;
                push(sharedData->queue, ev);
            }
            else
            {
                fprintf(stderr, "ERROR: Nucleo is computing, either abort or wait for it to finish\r\n");
            }
            break;
        default:
            break;
        };
        q = sharedData->quit;
        pthread_mutex_unlock(&mtx);
    }
    event ev = {.type = EV_KEYBOARD};
    pthread_mutex_lock(&mtx);
    pthread_cond_wait(&push_ok, &mtx);
    push(sharedData->queue, ev);
    pthread_mutex_unlock(&mtx);
    return 0;
}
void *serialThread(void *v)
{
    Data *sharedData = (Data *)v;
    bool q = false;
    int fd = serial_open(SERIAL_NAME);
    if (fd == -1)
    { // read from serial port
        q = true;
        event ev = {.type = EV_SERIAL};
        pthread_mutex_lock(&mtx);
        pthread_cond_wait(&push_ok, &mtx);
        push(sharedData->queue, ev);
        sharedData->quit = true;
        pthread_mutex_unlock(&mtx);
        return 0;
    }
    struct pollfd ufdr[1];
    ufdr[0].fd = fd;
    ufdr[0].events = POLLIN;
    uint8_t *buffer = malloc(BUF_SIZE);
    int buf_in = 0;
    while (!q)
    {
        pthread_mutex_lock(&mtx);
        q = sharedData->quit;
        pthread_mutex_unlock(&mtx);
        bool read_all_char = false;
        int msg_len = 0;
        bool ok1 = false;

        while (!read_all_char)
        {
            if ((poll(&ufdr[0], 1, 50) > 0) && (ufdr[0].revents & (POLLIN)))
            {
                uint8_t ch = serial_getc(fd);
                if (ch != -1)
                {
                    buffer[buf_in] = ch;
                    buf_in++;
                    if (buffer[0] == 10)
                    {
                        if (buf_in > 3)
                        {
                            uint16_t burst_data;
                            memcpy(&(burst_data), &(buffer[2 + 0 * sizeof(uint16_t)]), sizeof(uint16_t));
                            ok1 = get_message_size(buffer[0], burst_data, &msg_len);
                        }
                    }
                    else
                    {
                        ok1 = get_message_size(buffer[0], 0, &msg_len);
                    }
                }
            }
            else
            {
                read_all_char = true;
            }
            if (buf_in == msg_len && buf_in != 0)
            {
                read_all_char = true;
            }
        }
        uint16_t burst_data = (buffer[3] << 8) | buffer[2];
        message msg;
        if (buf_in == msg_len && read_all_char && buf_in != 0)
        {
            bool ok = parse_message_buf(buffer, burst_data, msg_len, &msg);

            if (ok && ok1)
            {
                buf_in = 0;
                event ev = {.msg = msg};
                switch (msg.type)
                {
                case MSG_ABORT:
                    ev.type = EV_ABORT_NUCLEO;
                    break;
                case MSG_VERSION:
                    ev.type = EV_VERSION;
                    break;
                case MSG_STARTUP:
                    ev.type = EV_STARTUP;
                    break;
                case MSG_OK:
                    ev.type = EV_OK;
                    break;
                case MSG_ERROR:
                    ev.type = EV_ERROR;
                    break;
                case MSG_DONE:
                    ev.type = EV_DONE;
                    break;
                case MSG_COMPUTE_DATA:
                    ev.type = EV_COMPUTE_DATA;
                    break;
                case MSG_COMPUTE_DATA_BURST:
                    ev.type = EV_COMPUTE_DATA_BURST;
                    break;
                default:
                    break;
                }
                pthread_mutex_lock(&mtx);
                pthread_cond_wait(&push_ok, &mtx);
                push(sharedData->queue, ev);
                pthread_mutex_unlock(&mtx);
            }
            else
            {
                fprintf(stderr, "ERROR: Cannot parse message type %d\r\n", buffer[0]);
            }
        }
    }
    free(buffer);
    event ev = {.type = EV_SERIAL};
    pthread_mutex_lock(&mtx);
    push(sharedData->queue, ev);
    pthread_mutex_unlock(&mtx);
    return 0;
}
void *computeThread(void *v)
{
    Data *sharedData = (Data *)v;
    bool q = false;
    pthread_mutex_lock(&mtx);
    bool computing;
    pthread_mutex_unlock(&mtx);
    while (!q)
    {
        pthread_mutex_lock(&mtx_compute);
        pthread_cond_wait(&receive_compute_data, &mtx_compute);
        computing = sharedData->computing;
        q = sharedData->quit;
        int get_index = sharedData->idx;
        int chunk_id = sharedData->chunk_id;
        int column = sharedData->column;
        double step_re = sharedData->step_re;
        double step_im = sharedData->step_im;
        int row = sharedData->row;
        double start_re = sharedData->start_re;
        double start_im = sharedData->start_im;
        pthread_mutex_unlock(&mtx_compute);
        if (computing && get_index != column * row && !q)
        {
            double re = start_re + (get_index % column) * step_re;
            double im = start_im + ceil(get_index / (double)column) * step_im;
            int n_re = get_index + 250 > row * column ? column * row - get_index : 250;
            n_re = get_index%column+n_re>column ? column-get_index%column : n_re;
            //printf("Sending new chunk, start_re = %lf, start_im = %lf, length = %d, step_re is %lf and step_im is %lf and start_re is %lf\r\n",re,im,n_re,step_re,step_im,start_re);
            message msg = {.type = MSG_COMPUTE, .data.compute.cid = chunk_id, .data.compute.re = re, .data.compute.im = im, .data.compute.n_im = 1, .data.compute.n_re = n_re};
            uint8_t *buf = malloc(BUF_SIZE);
            sendMessage(&msg, buf);
            free(buf);
        }
    }
    event ev = {.type = EV_COMPUTE_THREAD};
    pthread_mutex_lock(&mtx);
    push(sharedData->queue, ev);
    pthread_mutex_unlock(&mtx);
    return 0;
}
void sendMessage(message *msg, uint8_t *buf)
{
    int msg_len = 0;
    int fd = serial_open(SERIAL_NAME);
    bool ok = fill_message_buf(msg, buf, sizeof(message), &msg_len);
    if (ok)
    {
        for (int i = 0; i < msg_len; i++)
        {
            serial_putc(fd, buf[i]);
        }
    }
    else
    {
        fprintf(stderr, "ERROR: Cannot send message\r\n");
    }
}