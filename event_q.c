#include "event_q.h"

#include <stdlib.h>
#include <stdio.h>
#define EVENT_Q_CAPACITY 15
queue *create_queue()
{
    queue *queue = malloc(sizeof(*queue));
    queue->start=-1;
    queue->head=-1;
    queue->size=0;
    queue->capacity = EVENT_Q_CAPACITY;
    queue->values = malloc(EVENT_Q_CAPACITY * sizeof(event));
    return queue;
}
event pop(queue *q)
{
    if (q->size>0) //event in q
    {
        event ev = q->values[(++q->start)%EVENT_Q_CAPACITY];
        q->size -=1;
        return ev;
    }else //no events in q 
    {
        event ev = {.type=EV_NULL};
        return ev;
    }

}
void push(queue *q, event ev)
{
    if(q->size==q->capacity) //queue je plne
    {
        printf("\r\nQueue je plne");
    }else{ //v queue je misto
        q->values[(++q->head)%EVENT_Q_CAPACITY] = ev;
        q->size++;
    }
}
void clear_queue(queue *q){
    free(q->values);
    free(q);
}
