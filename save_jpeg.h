#ifndef __SAVE_JPEG_H__
#define __SAVE_JPEG_H__

/*
 * save image img to the file fname as jpeg
 * img is a sequence of R,G,B values of the image
 * starting from the top left corner, going from left to right
 */
void save_image_jpeg(int w, int h, unsigned char *img, char *fname);
void jpeg_compress(const unsigned char* src, int src_len, int w, int h, unsigned char** dst, unsigned long *dst_len, int quality, int components);

#endif

/* end of save_jpeg.h */