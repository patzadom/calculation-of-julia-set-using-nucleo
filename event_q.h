#include <stdint.h>
#include <stdbool.h>
#include "messages.h"
#define STARTUP_MSG_LEN 9
typedef enum
{
    EV_ABORT_KEYBOARD,
    EV_ABORT_NUCLEO,
    EV_VERSION,
    EV_GET_VERSION,
    EV_KEYBOARD,
    EV_SERIAL,
    EV_OK,
    EV_DONE,
    EV_SET_SIZE,
    EV_REDRAW,
    EV_SAVE_IMAGE,
    EV_DELETE_RESULT,
    EV_ERROR,
    EV_QUIT,
    EV_COMPUTE_THREAD,
    EV_NULL,
    EV_COMPUTE,
    EV_STARTUP,
    EV_COMPUTE_DATA,
    EV_COMPUTE_DATA_BURST,
    EV_RESET,
    EV_SET_COMPUTE
} event_type;
typedef struct
{
    uint8_t type;
    message msg;

} event;
typedef struct{
    int start;
    int head;
    int size;
    int capacity;
    event *values;
} queue;
event pop(queue*queue);
void push(queue *queue,event ev);
void clear_queue(queue *q);
queue *create_queue();