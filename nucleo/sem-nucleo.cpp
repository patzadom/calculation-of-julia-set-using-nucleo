#include "mbed.h"
#include "messages.h"
#include <math.h>
#define BUF_SIZE 510
#define COMPUTE_TIME 200
#define TICKER_PERIOD 0.5
Serial serial(SERIAL_TX,SERIAL_RX);
DigitalOut led(LED1);
void Tx_interrupt();
void Rx_interrupt();

bool receive_message(uint8_t *msg_buf,int size, int *len);
bool send_buffer(const uint8_t* msg, unsigned int size);
bool send_message(const message *msg, uint8_t *msg_buf, int size, int msg_len);

char rx_buffer[BUF_SIZE];
char tx_buffer[BUF_SIZE];

// pointers to the circular buffers
volatile int tx_in = 0;
volatile int tx_out = 0;
volatile int rx_in = 0;
volatile int rx_out = 0;
typedef struct {
    double re;
    double im;
} complex_number;
typedef struct {
    int cells_calculated;
    int row;
    int column;
} cell;
typedef struct {
    msg_set_compute set_msg;
    msg_compute compute_msg;
    cell cell_param;
    int chunk_id;
    bool computing;
} computeParameters;
computeParameters init()
{

    computeParameters nucleo_data;
    msg_set_compute set_msg;
    cell cell_param;
    msg_compute compute_msg;
    cell_param.row = cell_param.column = 1;
    cell_param.cells_calculated = set_msg.c_re = set_msg.c_im = set_msg.d_re = set_msg.d_im = set_msg.n = compute_msg.cid = compute_msg.re = compute_msg.im = compute_msg.n_re = compute_msg.n_im = nucleo_data.computing = 0;
    nucleo_data.set_msg = set_msg;
    nucleo_data.chunk_id=0;
    nucleo_data.compute_msg = compute_msg;
    nucleo_data.cell_param=cell_param;
    return nucleo_data;
}
InterruptIn button_event(USER_BUTTON);
volatile bool abort_request = false;

Ticker ticker;
void flip()
{
    led = !led;
}
void button()
{
    abort_request = true;
}
msg_compute abortCompute()
{
    msg_compute msg;
    msg.cid =0;
    msg.re=0;
    msg.im=0;
    msg.n_re=0;
    msg.n_im=0;
    return msg;
}
cell abortCellData()
{
    cell c;
    c.cells_calculated=0;
    c.column=1;
    c.row=1;
    return c;
}
int main()
{
    computeParameters nucleo_data = init();
    button_event.rise(&button);
    message compute_msg;
    uint8_t *compute_buf = (uint8_t*)malloc(BUF_SIZE);
    serial.baud(115200);
    serial.attach(&Tx_interrupt, Serial::TxIrq);
    message msg= { .type=MSG_STARTUP, .data.startup.message = {'p','a','t','z','a','d','o','m','\0'} };
    complex_number start;
    uint8_t msg_buf [sizeof(uint8_t)*250];
    int msg_len;
    while(serial.readable()) {
        serial.getc();
    }
    bool sent = send_message(&msg,msg_buf,sizeof(message),msg_len);
    msg_len=0;
    serial.attach(&Rx_interrupt,Serial::RxIrq);
    while(1) {
        if(abort_request) {
            if(nucleo_data.computing) {
                msg.type=MSG_ABORT;
                send_message(&msg,msg_buf,sizeof(message),msg_len);
                nucleo_data.compute_msg = abortCompute();
                nucleo_data.cell_param = abortCellData();
                nucleo_data.chunk_id=0;
                ticker.detach();
                led=0;
            }
            abort_request=false;
        }//end if abort_request
        if(rx_in != rx_out) {
            if(receive_message(msg_buf,sizeof(message),&msg_len)) {
                if(parse_message_buf(msg_buf,0,msg_len,&msg)) { //theres message in buffer, gotta read
                    switch(msg.type) {
                        case MSG_GET_VERSION:
                            msg.type=MSG_VERSION;
                            msg.data.version.major=1;
                            msg.data.version.minor=0;
                            msg.data.version.patch=0;
                            bool ok1 = send_message(&msg,msg_buf,sizeof(message),msg_len);
                            break;
                        case MSG_ABORT:
                            nucleo_data.computing=false;
                            msg.type=MSG_OK;
                            nucleo_data.compute_msg = abortCompute();
                            nucleo_data.cell_param = abortCellData();
                            nucleo_data.chunk_id=0;
                            bool ok2 = send_message(&msg,msg_buf,sizeof(message),msg_len);
                            ticker.detach();
                            led=0;
                            break;
                        case MSG_COMPUTE:
                            if(msg.data.compute.n_re>0 && msg.data.compute.n_im>0 && nucleo_data.set_msg.n!=0) {
                                nucleo_data.compute_msg = msg.data.compute;
                                nucleo_data.computing =true;
                                start.re = msg.data.compute.re;
                                start.im = msg.data.compute.im;
                                ticker.attach(&flip,TICKER_PERIOD);
                                msg.type = MSG_OK;
                                bool ok3 = send_message(&msg,msg_buf,sizeof(message),msg_len);
                            } else {
                                msg.type = MSG_ERROR;
                                bool ok3 = send_message(&msg,msg_buf,sizeof(message),msg_len);
                            }
                            break;
                        case MSG_SET_COMPUTE:
                            if(msg.data.set_compute.n>0) {
                                nucleo_data.set_msg = msg.data.set_compute;
                                msg.type = MSG_OK;
                                bool ok3 = send_message(&msg,msg_buf,sizeof(message),msg_len);

                            } else {
                                msg.type = MSG_ERROR;
                                bool ok3 = send_message(&msg,msg_buf,sizeof(message),msg_len);
                            }
                            break;
                        default:
                            break;
                    }//end switch

                } else { //error with sending message
                    msg.type = MSG_ERROR;
                    send_message(&msg,msg_buf,sizeof(message),msg_len);
                }
            }//end receive
            //end rx_in!=rx_out

        } else if(nucleo_data.computing) {
            if(nucleo_data.cell_param.cells_calculated<nucleo_data.compute_msg.n_re) {
                int iter_number = 0;
                bool quit = false;
                complex_number next = start;
                while(iter_number<nucleo_data.set_msg.n && !quit) {
                    if(sqrt(next.re*next.re + next.im*next.im)>2) {
                        quit=true;
                    } else {
                        double tmp = next.re;
                        next.re = next.re*next.re - next.im*next.im + nucleo_data.set_msg.c_re;
                        next.im = tmp*next.im*2 + nucleo_data.set_msg.c_im;
                        iter_number +=1;
                    }
                }

                compute_buf[nucleo_data.cell_param.cells_calculated]=iter_number;
                nucleo_data.cell_param.cells_calculated++;
                start.re+= nucleo_data.set_msg.d_re;

            } else {
                msg.type = MSG_COMPUTE_DATA_BURST;
                msg.data.compute_data_burst.chunk_id = nucleo_data.compute_msg.cid;
                msg.data.compute_data_burst.length = nucleo_data.cell_param.cells_calculated;
                msg.data.compute_data_burst.iters = compute_buf;
                bool ok = send_message(&msg,msg_buf,sizeof(message),msg_len);
                nucleo_data.computing = false;
                nucleo_data.compute_msg = abortCompute();
                nucleo_data.cell_param = abortCellData();
                nucleo_data.chunk_id=0;
                ticker.detach();
                led=0;
                if(!ok) {
                    msg.type = MSG_ERROR;
                    send_message(&msg,msg_buf,sizeof(message),msg_len);
                }
            }//end of nucleo_data.computing
        }

    }

}
void Rx_interrupt()
{
    // receive bytes and stop if rx_buffer is full
    while ((serial.readable()) && (((rx_in + 1) % BUF_SIZE) != rx_out)) {
        rx_buffer[rx_in] = serial.getc();
        rx_in = (rx_in + 1) % BUF_SIZE;
    }
    return;
}

void Tx_interrupt()
{
    // send a single byte as the interrupt is triggered on empty out buffer
    if (tx_in != tx_out) {
        serial.putc(tx_buffer[tx_out]);
        tx_out = (tx_out + 1) % BUF_SIZE;
    } else { // buffer sent out, disable Tx interrupt
        USART2->CR1 &= ~USART_CR1_TXEIE; // disable Tx interrupt
    }
    return;
}

bool receive_message(uint8_t *msg_buf,int size, int *len)
{
    bool ret = false;
    int i = 0;
    *len = 0; // message size
    NVIC_DisableIRQ(USART2_IRQn); // start critical section for accessing global data
    while ( ((i == 0) || (i != *len)) && i < size ) {
        if (rx_in == rx_out) { // wait if buffer is empty
            NVIC_EnableIRQ(USART2_IRQn); // enable interrupts for receing buffer
            while (rx_in == rx_out) { // wait of next character
            }
            NVIC_DisableIRQ(USART2_IRQn); // disable interrupts for accessing global buffer
        }
        uint8_t c = rx_buffer[rx_out];
        if (i == 0) { // message type
            if (get_message_size(c,0, len)) { // message type recognized
                msg_buf[i++] = c;
                ret = *len <= size; // msg_buffer must be large enough
            } else {
                ret = false;
                break; // unknown message
            }
        } else {
            msg_buf[i++] = c;
        }
        rx_out = (rx_out + 1) % BUF_SIZE;
    }
    NVIC_EnableIRQ(USART2_IRQn); // end critical section
    return ret;

}

bool send_buffer(const uint8_t* msg, unsigned int size)
{
    if (!msg && size == 0) {
        return false;    // size must be > 0
    }
    int i = 0;
    NVIC_DisableIRQ(USART2_IRQn); // start critical section for accessing global data
    USART2->CR1 |= USART_CR1_TXEIE; // enable Tx interrupt on empty out buffer
    bool empty = (tx_in == tx_out);
    while ( (i == 0) || i < size ) { //end reading when message has been read
        if ( ((tx_in + 1) % BUF_SIZE) == tx_out) { // needs buffer space
            NVIC_EnableIRQ(USART2_IRQn); // enable interrupts for sending buffer
            while (((tx_in + 1) % BUF_SIZE) == tx_out) {
                /// let interrupt routine empty the buffer
            }
            NVIC_DisableIRQ(USART2_IRQn); // disable interrupts for accessing global buffer
        }
        tx_buffer[tx_in] = msg[i];
        i += 1;
        tx_in = (tx_in + 1) % BUF_SIZE;
    } // send buffer has been put to tx buffer, enable Tx interrupt for sending it out
    USART2->CR1 |= USART_CR1_TXEIE; // enable Tx interrupt
    NVIC_EnableIRQ(USART2_IRQn); // end critical section
    return true;
}
bool send_message(const message *msg, uint8_t *msg_buf, int size, int msg_len)
{
    return fill_message_buf(msg,msg_buf,sizeof(message),&msg_len) && send_buffer(msg_buf,msg_len);
}